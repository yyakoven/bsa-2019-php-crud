<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Order;
use App\OrderItem;
use App\Product;

class OrderController extends Controller
{
    public function index()
    {
        return Order::all()->map(function($order) {
            return new OrderResource($order);
        });
    }

    public function store(Request $request)
    {
        $data = ($request->all());
        $order = new Order();
        $order->buyer_id = $data['buyerId'];
        $order->orderItems = sizeof($data['orderItems']);
        $order->save();
        $items = collect($data['orderItems'])->map(function($orderItem) use ($order){
            $item = new OrderItem();
            $item->order_id = $order->id;
            $item->product_id = $orderItem['productId'];
            $item->quantity = $orderItem['productQty'];
            $item->price = Product::find($orderItem['productId'])->price;
            $item->discount = (int)$orderItem['productDiscount'];
            $item->sum = round($item->price * ((100 - $item->discount) / 100) * $item->quantity);
            $item->save();
        });
        return new Response (new OrderResource($order));
    }

    public function show($id)
    {
        return new OrderResource(Order::findOrFail($id));
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $order = Order::find($id);
        $result = false;
        if ($order) {
            $order->orderItems()->delete();
            $result = Order::destroy($id);
        }
        return ['result' => $result ? 'success' : 'fail'];
    }
}
