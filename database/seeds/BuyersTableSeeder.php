<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BuyersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        factory(\App\Buyer::class, 10)->create()
        ->each(function ($buyer) use ($faker) {
            // random number of orders for each buyer
            $num_orders = $faker->numberBetween(1, 5);
            $buyer->update(['orders' => $num_orders]);
            $buyer->orders()->saveMany(
                factory(\App\Order::class, $num_orders)
                ->create(['buyer_id'=>$buyer->id])
                ->each(function ($order) use ($faker) {
                    //random number of ordered items for each order
                    $num_order_items = $faker->numberBetween(1, 5);
                    $order->update(['orderItems' => $num_order_items]);
                    $order->orderItems()->saveMany(
                    factory(\App\OrderItem::class, $num_order_items)
                    ->create(['order_id' => $order->id]));
                })
            );
        });
    }
}
