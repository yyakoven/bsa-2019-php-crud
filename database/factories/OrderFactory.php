<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        // 'date' => $faker->date(),
        'orderItems' => $faker->numberBetween(1, 10),
        'buyer_id' => $faker->numberBetween(1, 10),
        'created_at' => $faker->dateTimeBetween($startDate = '-3 years', $endDate = 'now')
    ];
});
