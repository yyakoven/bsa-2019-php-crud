<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    $price = $faker->numberBetween(200, 10000);
    $discount = $faker->numberBetween(1, 100);
    $quantity = $faker->numberBetween(1, 10);
    return [
        'order_id' => $faker->numberBetween(1, 10),
        'product_id' => $faker->numberBetween(1, 20),
        'quantity' => $quantity,
        'price' => $price,
        'discount' => $discount,
        'sum' => round($price * ((100 - $discount) / 100) * $quantity) 
    ];
});
